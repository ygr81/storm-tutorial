package net.vault7.common;

import net.vault7.CompositeMessageId;
import net.vault7.OutputFields;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;

import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class RandomStringIntSpout extends BaseRichSpout {

  private static final String[] OUTPUT_FIELDS = new String[]{OutputFields.STRING_REPR, OutputFields.INT_REPR};

  private SpoutOutputCollector _collector;
  private long lastTime;

  @Override
  public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
    _collector = spoutOutputCollector;
  }

  @Override
  public void nextTuple() {
    long currentTime = System.currentTimeMillis();

    if (currentTime - lastTime <= 5000) {
      return;
    }

    lastTime = currentTime;

    int valueToEmit = ThreadLocalRandom.current().nextInt();
    final String[] words = new String[]{"nathan", "mike", "jackson", "golda", "bertels"};
    final String word = words[ThreadLocalRandom.current().nextInt(words.length)];

    System.out.println(getClass().getSimpleName() + " emitting: " + word + " -> " + valueToEmit);

    _collector.emit(Arrays.asList(word, valueToEmit), new CompositeMessageId(ThreadLocalRandom.current().nextLong()));
//    _collector.emit(Arrays.asList(word, valueToEmit));
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields(OUTPUT_FIELDS));
  }

  @Override
  public void ack(Object msgId) {
    System.out.println(getClass().getName() + " ACK " + msgId);
  }

  @Override
  public void fail(Object msgId) {
    System.out.println(getClass().getName() + " FAIL " + msgId);
  }
}
