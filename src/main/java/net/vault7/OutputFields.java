package net.vault7;

public interface OutputFields {
  String STRING_REPR ="StringRepresentation";
  String INT_REPR = "IntegerRepresentation";
  String ADDRESS = "Address";
}
