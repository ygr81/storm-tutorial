package net.vault7.Storm03SplitStreams;

import net.vault7.OutputFields;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.Collections;
import java.util.Map;

class IntermediateBolt extends BaseRichBolt {
  private OutputCollector collector;

  @Override
  public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
    this.collector = collector;
  }

  @Override
  public void execute(Tuple input) {
    System.out.println("Intermediate " + input);

    int intVal = (int) input.getValueByField(OutputFields.INT_REPR);
    collector.emit(input, Collections.singletonList(intVal));

    collector.ack(input);
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields(OutputFields.INT_REPR));
  }
}
