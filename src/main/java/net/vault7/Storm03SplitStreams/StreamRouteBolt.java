package net.vault7.Storm03SplitStreams;

import net.vault7.OutputFields;
import net.vault7.StreamIds;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.Collections;
import java.util.Map;

class StreamRouteBolt extends BaseRichBolt {
  private OutputCollector collector;

  @Override
  public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
    this.collector = collector;
  }

  @Override
  public void execute(Tuple input) {
    System.out.println(input);

    int intVal = (int) input.getValueByField(OutputFields.INT_REPR);
    if (intVal % 2 == 0) {
      collector.emit(input, Collections.singletonList(intVal));
    } else {
      collector.emit(StreamIds.FAILURE_STREAM.name()
          , input, Collections.singletonList(intVal));
    }

    collector.ack(input);
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
    declarer.declare(new Fields(OutputFields.INT_REPR));
    declarer.declareStream(StreamIds.FAILURE_STREAM.name(), new Fields(OutputFields.INT_REPR));
  }
}
