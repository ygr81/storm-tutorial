package net.vault7.Storm03SplitStreams;

import net.vault7.LocalStormRunner;
import net.vault7.StreamIds;
import net.vault7.common.RandomStringIntSpout;

class SplitStreams extends LocalStormRunner {

  private static final String SPOUT_ID = "split-streams-spout";
  private static final String ROUTE_STREAM_BOLT_ID = "stream-route-bolt";
  private static final String AGGREGATE_STREAMS_BOLT_ID = "stream-aggregation-bolt";
  private static final String INTERMEDIATE_BOLT_ID = "intermediate-bolt";

  SplitStreams(String topologyName) {
    super(topologyName);
  }


  public static void main(String[] args) throws InterruptedException {
    new SplitStreams(SplitStreams.class.getSimpleName() + "-topology").run();
  }

  @Override
  protected void wireTopology() {
    builder.setSpout(SPOUT_ID, new RandomStringIntSpout(), 1);

    builder.setBolt(ROUTE_STREAM_BOLT_ID, new StreamRouteBolt(), 1).shuffleGrouping(SPOUT_ID);

    builder.setBolt(INTERMEDIATE_BOLT_ID + "-default", new IntermediateBolt(), 1)
        .shuffleGrouping(ROUTE_STREAM_BOLT_ID);
    builder.setBolt(INTERMEDIATE_BOLT_ID + "-failure-stream", new IntermediateBolt(), 1)
        .shuffleGrouping(ROUTE_STREAM_BOLT_ID, StreamIds.FAILURE_STREAM.name());

    builder.setBolt(AGGREGATE_STREAMS_BOLT_ID, new StreamAggregatorBolt(), 1)
        .shuffleGrouping(INTERMEDIATE_BOLT_ID + "-default")
        .shuffleGrouping(INTERMEDIATE_BOLT_ID + "-failure-stream");
  }

}