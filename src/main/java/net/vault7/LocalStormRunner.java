package net.vault7;

import org.apache.storm.Config;
import org.apache.storm.starter.util.StormRunner;
import org.apache.storm.topology.TopologyBuilder;

public abstract class LocalStormRunner {
  private static final int DEFAULT_RUNTIME_IN_SECONDS = 3600;
  protected final TopologyBuilder builder;
  private final String topologyName;
  private final Config topologyConfig;


  public LocalStormRunner(String topologyName) {
    this.topologyName = topologyName;
    builder = new TopologyBuilder();
    topologyConfig = createTopologyConfig();
    wireTopology();
  }

  private static Config createTopologyConfig() {
    Config config = new Config();
    config.setDebug(false);
    config.setMessageTimeoutSecs(3);
    return config;
  }

  protected abstract void wireTopology();

  protected final void run() throws InterruptedException {
    StormRunner.runTopologyLocally(builder.createTopology(), topologyName, topologyConfig, DEFAULT_RUNTIME_IN_SECONDS);
  }
}