package net.vault7.Storm04ReliableVsNonReliableProcessing;

import java.util.Arrays;

class NonReliableNonWait extends NonWaitRandomStringIntSpout {

  @Override
  void emit(String word, int valueToEmit) {
    _collector.emit(Arrays.asList(word, valueToEmit));
  }
}
