package net.vault7.Storm04ReliableVsNonReliableProcessing;

import net.vault7.CompositeMessageId;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

class ReliableNonWait extends NonWaitRandomStringIntSpout {

  @Override
  void emit(String word, int valueToEmit) {
    _collector.emit(
        Arrays.asList(word, valueToEmit), new CompositeMessageId(ThreadLocalRandom.current().nextLong()));
  }
}
