package net.vault7.Storm04ReliableVsNonReliableProcessing;

import net.vault7.OutputFields;
import net.vault7.StreamIds;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.Collections;
import java.util.Map;

class ReliableBolt extends BaseRichBolt {
  private OutputCollector collector;

  private long lastTime;
  private long counter;

  @Override
  public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
    this.collector = collector;
    lastTime = System.currentTimeMillis();
    counter = 0;
  }

  @Override
  public void execute(Tuple input) {

    long currentTime = System.currentTimeMillis();

    if (currentTime - lastTime >= 10_000) {
      System.out.println("RELIABLE COUNTER: " + counter);
      counter = 0;
      lastTime = currentTime;
    }



    int intVal = (int) input.getValueByField(OutputFields.INT_REPR);
    collector.ack(input);
    counter++;
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
  }
}
