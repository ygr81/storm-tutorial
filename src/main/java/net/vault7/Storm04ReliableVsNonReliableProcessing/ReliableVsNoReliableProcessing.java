package net.vault7.Storm04ReliableVsNonReliableProcessing;

import net.vault7.LocalStormRunner;

class ReliableVsNoReliableProcessing extends LocalStormRunner {

  private static final String SPOUT_ID = "split-streams-spout";
  private static final String ROUTE_STREAM_BOLT_ID = "stream-route-bolt";
  private static final String AGGREGATE_STREAMS_BOLT_ID = "stream-aggregation-bolt";
  private static final String INTERMEDIATE_BOLT_ID = "intermediate-bolt";

  ReliableVsNoReliableProcessing(String topologyName) {
    super(topologyName);
  }


  public static void main(String[] args) throws InterruptedException {
    new ReliableVsNoReliableProcessing(ReliableVsNoReliableProcessing.class.getSimpleName() + "-topology").run();
  }

  @Override
  protected void wireTopology() {
//    reliableTopology();
    nonReliableTopology();
  }


  private void reliableTopology() {
    builder.setSpout(SPOUT_ID, new ReliableNonWait(), 1);
    builder.setBolt(ROUTE_STREAM_BOLT_ID, new ReliableBolt(), 1).shuffleGrouping(SPOUT_ID);
  }

  private void nonReliableTopology() {
    builder.setSpout(SPOUT_ID, new NonReliableNonWait(), 1);
    builder.setBolt(ROUTE_STREAM_BOLT_ID, new NonReliableBolt(), 1).shuffleGrouping(SPOUT_ID);
  }
}
