package net.vault7.Storm04ReliableVsNonReliableProcessing;

import net.vault7.OutputFields;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

abstract class NonWaitRandomStringIntSpout extends BaseRichSpout {

  private static final String[] OUTPUT_FIELDS = new String[]{OutputFields.STRING_REPR,
      OutputFields.INT_REPR};

  SpoutOutputCollector _collector;

  @Override
  public void open(Map map, TopologyContext topologyContext,
      SpoutOutputCollector spoutOutputCollector) {
    _collector = spoutOutputCollector;
  }

  @Override
  public void nextTuple() {

    int valueToEmit = ThreadLocalRandom.current().nextInt();
    final String[] words = new String[]{"nathan", "mike", "jackson", "golda", "bertels"};
    final String word = words[ThreadLocalRandom.current().nextInt(words.length)];

    emit(word, valueToEmit);
  }

  abstract void emit(String word, int valueToEmit);

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields(OUTPUT_FIELDS));
  }
}
