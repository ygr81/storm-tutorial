package net.vault7.Storm04ReliableVsNonReliableProcessing;

import net.vault7.OutputFields;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.Map;

class NonReliableBolt extends BaseRichBolt {
  private OutputCollector collector;

  private long lastTime;
  private long counter;

  @Override
  public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
    this.collector = collector;
    lastTime = System.currentTimeMillis();
    counter = 0;
  }

  @Override
  public void execute(Tuple input) {

    long currentTime = System.currentTimeMillis();

    if (currentTime - lastTime >= 10_000) {
      System.out.println("NON RELIABLE COUNTER: " + counter);
      counter = 0;
      lastTime = currentTime;
    }


    int intVal = (int) input.getValueByField(OutputFields.INT_REPR);
    counter++;
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer declarer) {
  }
}
