package net.vault7.Storm02MicroBatching;

import net.vault7.LocalStormRunner;

class MicroBatching extends LocalStormRunner {

  private static final String SPOUT_ID = "micro-batching-spout";
  private static final String SIMPLE_BOLT_ID = "simple-bolt";
  private static final String BATCHING_BOLT_ID = "batching-bolt";

  MicroBatching(String topologyName) {
    super(topologyName);
  }


  public static void main(String[] args) throws InterruptedException {
    new MicroBatching(MicroBatching.class.getSimpleName() + "-topology").run();
  }

  @Override
  protected void wireTopology() {
    builder.setSpout(SPOUT_ID, new AddressSpout(), 1);

//    builder.setBolt(SIMPLE_BOLT_ID, new SimpleBolt(), 1).shuffleGrouping(SPOUT_ID);
//    builder.setBolt(BATCHING_BOLT_ID, new BatchBolt(), 1).shuffleGrouping(SPOUT_ID);
    builder.setBolt(BATCHING_BOLT_ID, new TickTupleBatchBolt(), 1).shuffleGrouping(SPOUT_ID);
  }

}
