package net.vault7.Storm02MicroBatching;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;
import net.vault7.OutputFields;
import org.apache.storm.Config;
import org.apache.storm.Constants;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

class BatchBolt extends BaseRichBolt {

  private OutputCollector _collector;
  private MongoDbService mongoDbService;
  private int tickCounter;
  private long timeSummary;
  private long lastBatchProcessTimeSeconds;
  private long batchIntervalInSecs = 1;
  private int counter;
  private LinkedBlockingQueue<Tuple> queue = new LinkedBlockingQueue<>();
  private int batchSize = 1000;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    _collector = outputCollector;
    mongoDbService = new MongoDbService();
  }

  private void tickTupleHandler() {
    if (shouldFlush()) {
      finishBatch();
    }
  }

  private void dataTupleHandler(Tuple tuple) {
    queue.add(tuple);
    int queueSize = queue.size();
    if (queueSize >= batchSize) {
      finishBatch();
    }
  }

  @Override
  public void execute(Tuple tuple) {
    if (isTickTuple(tuple)) {
      tickTupleHandler();
    } else {
      dataTupleHandler(tuple);
    }
  }

//  @Override
//  public void execute2(Tuple tuple) {
//    if (isTickTuple(tuple)) {
//      tickCounter++;
//      if (shouldFlush()) {
//        finishBatch();
//        System.out.println("tickCounter: " + tickCounter);
//        System.out.println("time: " + timeSummary / 1_000_000);
//      }
//
//    } else {
//      counter++;
//      long startTime = System.nanoTime();
//      queue.add(tuple);
//      int queueSize = queue.size();
//      long stopTime = System.nanoTime();
//
//      timeSummary += (stopTime - startTime);
//      if (queueSize >= batchSize) {
//        finishBatch();
//      }
//
//      if (counter % 1_000 == 0) {
//        System.out.println("counter: " + counter);
//        System.out.println("time: " + timeSummary / 1_000_000);
//      }
//    }
//  }

  private void finishBatch() {
    lastBatchProcessTimeSeconds = currentTimeInSecs();

    if (queue.size() == 0) {
      return;
    }

    long startTime = System.nanoTime();
    List<Tuple> tuples = new ArrayList<>();
    queue.drainTo(tuples);
    mongoDbService.insertAdresses(
        tuples.stream().map((t) -> (Address) t.getValueByField(OutputFields.ADDRESS))
            .collect(Collectors.toList()));

    for (Tuple tuple : tuples) {
      _collector.ack(tuple);
    }

    long stopTime = System.nanoTime();
    timeSummary += (stopTime - startTime);
  }

  private boolean shouldFlush() {
    return (currentTimeInSecs() - lastBatchProcessTimeSeconds) >= batchIntervalInSecs;
  }

  private static long currentTimeInSecs() {
    return System.currentTimeMillis() / 1_000;
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

  }

  @Override
  public Map<String, Object> getComponentConfiguration() {
    Config config = new Config();
    config.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 1);
    return config;
  }

  private static boolean isTickTuple(Tuple tuple) {
    return tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID)
        && tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID);
  }
}
