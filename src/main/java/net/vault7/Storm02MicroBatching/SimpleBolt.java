package net.vault7.Storm02MicroBatching;

import net.vault7.OutputFields;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Tuple;

import java.util.Map;

class SimpleBolt extends BaseRichBolt {

  private OutputCollector outputCollector;
  private MongoDbService mongoDbService;
  private int counter;
  private long timeSummary;


  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    this.outputCollector = outputCollector;
    this.mongoDbService = new MongoDbService();
  }

  @Override
  public void execute(Tuple tuple) {
    counter++;
    long startTime = System.nanoTime();
    Address address = (Address) tuple.getValueByField(OutputFields.ADDRESS);
    mongoDbService.insertAddress(address);
    outputCollector.ack(tuple);
    long stopTime = System.nanoTime();

    timeSummary += (stopTime - startTime);;
    if (counter % 1_000 == 0) {
      System.out.println("counter: " + counter);
      System.out.println("time summary: " + timeSummary/1_000_000);
    }
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {

  }
}
