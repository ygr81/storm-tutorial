package net.vault7.Storm02MicroBatching;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;

class AddressBuilder {
  static Address build() {
    Address a = new Address();
    a.setId(generateRandom());
    a.setInsertedTime(LocalDateTime.now());
    a.setStreet(generateRandom());
    a.setNumber(generateRandom());
    a.setZipCode(generateRandom());
    a.setCity(generateRandom());
    a.setState(generateRandom());
    a.setCountryCode(generateRandom());
    a.setContinentCode(generateRandom());
    return a;
  }

  private static String generateRandom() {
    int leftLimit = 97; // letter 'a'
    int rightLimit = 122; // letter 'z'
    int targetStringLength = 10;
    StringBuilder buffer = new StringBuilder(targetStringLength);
    for (int i = 0; i < targetStringLength; i++) {
      int randomLimitedInt = leftLimit + (int)
          (ThreadLocalRandom.current().nextFloat() * (rightLimit - leftLimit + 1));
      buffer.append((char) randomLimitedInt);
    }
    String generatedString = buffer.toString();
    return generatedString;
  }
}

@Document(collection = "Addresses")
@CompoundIndexes({
    @CompoundIndex(name = "compos", def = "{'street':1, 'number':-1}")
})
class Address implements Serializable {

  @Id
  private String id;
  private LocalDateTime insertedTime;
  @Indexed
  private String street;
  private String number;
  private String zipCode;
  private String city;
  private String state;
  private String countryCode;
  private String continentCode;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public LocalDateTime getInsertedTime() {
    return insertedTime;
  }

  public void setInsertedTime(LocalDateTime insertedTime) {
    this.insertedTime = insertedTime;
  }

  public String getStreet() {
    return street;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public String getNumber() {
    return number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getZipCode() {
    return zipCode;
  }

  public void setZipCode(String zipCode) {
    this.zipCode = zipCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public String getContinentCode() {
    return continentCode;
  }

  public void setContinentCode(String continentCode) {
    this.continentCode = continentCode;
  }
}
