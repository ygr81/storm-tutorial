package net.vault7.Storm02MicroBatching;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

class MongoDbService {

  private MongoTemplate mongoTemplate;
  private MongoClient mongoClient;

  public MongoDbService() {
    MongoClientOptions.Builder builder = new MongoClientOptions.Builder();

    builder.writeConcern(WriteConcern.ACKNOWLEDGED).maxConnectionIdleTime(600_000).socketKeepAlive(true);
    List<ServerAddress> serverAddressList = Arrays.asList(new ServerAddress("localhost:27017"));

    this.mongoClient = new MongoClient(serverAddressList, builder.build());
    this.mongoTemplate = new MongoTemplate(mongoClient, "my-database");
  }

  public void insertAddress(Address trade) {
    mongoTemplate.insert(trade);
  }

  public MongoTemplate getMongoTemplate() {
    return mongoTemplate;
  }

  public void insertAdresses(Collection<? extends Address> batchToSave) {
    mongoTemplate.insert(batchToSave, Address.class);
  }
}
