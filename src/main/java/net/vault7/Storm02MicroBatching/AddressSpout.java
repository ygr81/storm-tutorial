package net.vault7.Storm02MicroBatching;

import net.vault7.CompositeMessageId;
import net.vault7.OutputFields;
import org.apache.storm.spout.SpoutOutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichSpout;
import org.apache.storm.tuple.Fields;
import org.apache.storm.utils.Utils;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

class AddressSpout extends BaseRichSpout {
  private static final String[] OUTPUT_FIELDS = new String[]{OutputFields.ADDRESS};

  private SpoutOutputCollector _collector;
  private long start;
  private long stop;
  private int counter;

  @Override
  public void open(Map map, TopologyContext topologyContext, SpoutOutputCollector spoutOutputCollector) {
    _collector = spoutOutputCollector;
  }

  @Override
  public void nextTuple() {
    if(counter>1_000_000) {
      return;
    }

    if (start == 0) {
      System.out.println("Waiting....");
      Utils.sleep(5_000);
      start = System.nanoTime();
      System.out.println("GO!");
    }

    _collector.emit(Collections.singletonList(AddressBuilder.build()),
        new CompositeMessageId(ThreadLocalRandom.current().nextLong()));

    counter++;
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields(OUTPUT_FIELDS));
  }

  @Override
  public void ack(Object msgId) {
    stop = System.nanoTime();
  }

  @Override
  public void fail(Object msgId) {
    stop = System.nanoTime();
  }
}
