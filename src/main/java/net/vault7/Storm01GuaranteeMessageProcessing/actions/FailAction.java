package net.vault7.Storm01GuaranteeMessageProcessing.actions;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Tuple;

import java.util.List;

public class FailAction implements Action {
  @Override
  public void execute(OutputCollector outputCollector, Tuple anchor, List<Object> tuple) {
    System.out.println("======= FAIL " + anchor.getMessageId());
    outputCollector.fail(anchor);
  }

  @Override
  public String toString() {
    return "FAIL";
  }
}
