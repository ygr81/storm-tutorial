package net.vault7.Storm01GuaranteeMessageProcessing.actions;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Tuple;

import java.util.List;

public class EmitAndAckAction implements Action {
  @Override
  public void execute(OutputCollector outputCollector, Tuple anchor, List<Object> tuple) {
    System.out.println("======= EMIT AND ACTION " + anchor.getMessageId());
    outputCollector.emit(anchor, tuple);
    outputCollector.ack(anchor);
  }

  @Override
  public String toString() {
    return "EMIT & ACK";
  }
}
