package net.vault7.Storm01GuaranteeMessageProcessing.actions;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Tuple;

import java.io.Serializable;
import java.util.List;

public interface Action extends Serializable {
  void execute(OutputCollector outputCollector, Tuple anchor, List<Object> tuple);
}
