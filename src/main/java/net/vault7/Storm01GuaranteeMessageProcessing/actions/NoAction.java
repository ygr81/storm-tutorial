package net.vault7.Storm01GuaranteeMessageProcessing.actions;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.tuple.Tuple;

import java.util.List;

public class NoAction implements Action {
  @Override
  public void execute(OutputCollector outputCollector, Tuple anchor, List<Object> tuple) {

  }

  @Override
  public String toString() {
    return "NOP";
  }
}
