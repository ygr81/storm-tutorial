package net.vault7.Storm01GuaranteeMessageProcessing;

import net.vault7.LocalStormRunner;
import net.vault7.Storm01GuaranteeMessageProcessing.actions.*;
import net.vault7.common.RandomStringIntSpout;

/**
 * approach to handle failures:
 *
 * if failures are sometimes expected and it's possible to do a retry, then definetely some retry policy can be
 * introduced. E.g. when invoking some URLs: try up to 5 times before failing.
 *
 * Storm doesn't reply tuples itself. What Storm does is provide the infrastructure to tell spouts if emitted tuples
 * have been successfully processed or not (calling ack/fail on spout respectively). It's responsibility to
 * spout's developer to define what to do when tuple failed. Replay it automatically, do some error handling?
 */
class GuaranteeMessageProcessing extends LocalStormRunner {

  private static final String SPOUT_ID = "guarantee-message-processing-spout";
  private static final String FIRST_BOLT_ID = "first-bolt";
  private static final String SECOND_BOLT_ID = "second-bolt";

  private GuaranteeMessageProcessing(String topologyName) {
    super(topologyName);
  }

  public static void main(String[] args) throws InterruptedException {
    new GuaranteeMessageProcessing("guarantee-message-processing-topology").run();
  }

  @Override
  protected void wireTopology() {
    builder.setSpout(SPOUT_ID, new RandomStringIntSpout(), 1);

    // TODO: before acking/failing emit tuple for further processing!!!
//    actionsToBolts(new EmitAndFailAction(), new FailAction());
//    actionsToBolts(new EmitAndFailAction(), new AckAction());
//    actionsToBolts(new EmitAndFailAction(), new NoAction());
//
    actionsToBolts(new EmitAndAckAction(), new FailAction());
//    actionsToBolts(new EmitAndAckAction(), new AckAction());
//    actionsToBolts(new EmitAndAckAction(), new NoAction());
//    actionsToBolts(new EmitAndAckAction(), new ReportErrorAction());

//    actionsToBolts(new EmitAction(), new AckAction());
//    actionsToBolts(new EmitAction(), new FailAction());
//    actionsToBolts(new EmitAction(), new NoAction());
//     actionsToBolts(new AckAndEmitAction(), new FailAction());
  }




  private void actionsToBolts(Action firstAction, Action secondAction) {
    builder.setBolt(FIRST_BOLT_ID, new ActionBolt(firstAction), 1)
        .shuffleGrouping(SPOUT_ID);

    builder.setBolt(SECOND_BOLT_ID, new ActionBolt(secondAction), 1)
        .shuffleGrouping(FIRST_BOLT_ID);
  }

}
