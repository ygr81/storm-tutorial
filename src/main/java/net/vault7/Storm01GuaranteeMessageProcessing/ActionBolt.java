package net.vault7.Storm01GuaranteeMessageProcessing;

import net.vault7.OutputFields;
import net.vault7.Storm01GuaranteeMessageProcessing.actions.Action;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.Arrays;
import java.util.Map;

class ActionBolt extends BaseRichBolt {

  private final Action action;

  ActionBolt(final Action action) {
    this.action = action;
  }

  @Override
  public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
    outputFieldsDeclarer.declare(new Fields(OutputFields.STRING_REPR, OutputFields.INT_REPR));
  }

  protected OutputCollector outputCollector;

  @Override
  public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
    this.outputCollector = outputCollector;
  }

  @Override
  public void execute(Tuple tuple) {
    String stringRepr = (String) tuple.getValueByField(OutputFields.STRING_REPR);
    int intRepr = (int) tuple.getValueByField(OutputFields.INT_REPR);

    System.out.println(action + " bolt " + stringRepr + " -> " + intRepr);

    action.execute(outputCollector, tuple, Arrays.asList(stringRepr, intRepr));
  }
}
