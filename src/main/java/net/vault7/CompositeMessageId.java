package net.vault7;

import java.io.Serializable;
import java.util.Objects;

public class CompositeMessageId implements Serializable {
  private final Long messageId;

  public CompositeMessageId(Long messageId) {
    this.messageId = messageId;
  }

  Long getMessageId() {
    return messageId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    CompositeMessageId that = (CompositeMessageId) o;
    return Objects.equals(messageId, that.messageId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(messageId);
  }

  @Override
  public String toString() {
    return "CompositeMessageId{" +
        "messageId=" + messageId +
        '}';
  }
}
